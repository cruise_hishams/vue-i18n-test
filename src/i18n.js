import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from './locales/en'
import ge from './locales/ge'
import fr from './locales/fr'

Vue.use(VueI18n)

const messages =
    {
      en,
      ge,
      fr
    };

export default new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages
})
