export default {
    nav: {
        home: 'Zuhause',
        lines: 'Zeilen',
        contactUs: 'kontaktiere uns'
    },
    message: {
        title: 'das ist ein titel',
        paragraph: 'Dies ist ein Absatz',
        email: 'Mailen Sie uns an:',
        call: 'oder rufen Sie uns an:'
    },
    lines: {
        pando: {
            name: 'P&O Cruises',
            desc: 'P & O Cruises kombiniert neueste Innovationen mit jahrhundertelanger Kreuzfahrtkompetenz und bietet mehr Kreuzfahrten ab Southampton als jede britische Kreuzfahrtlinie.'
        },
        royal: {
            name: 'Royal Caribbean',
            desc: 'RCI: die jüngste und innovativste Kreuzfahrtflotte der Welt. Mit einer Auswahl von mehr als 20 Schiffen können Kreuzfahrten zu mehr als 130 Zielen durchgeführt werden.'
        },
        princess: {
            name: 'Princess Cruises',
            desc: 'Mit ihrem freundlichen, amerikanischen Stil kann man sich um jeden Wunsch kümmern und die Gedanken des Alltags zuhause werden bald schmelzen.'
        },
        cunard: {
            name: 'Cunard Line',
            desc: 'Cunard bietet seit 1840 zeitlose Reisen und ist zu einem Begriff für Komfort und Stil geworden. Der legendäre Cunard wird Ihre Erwartungen übertreffen.'
        }

    }
}