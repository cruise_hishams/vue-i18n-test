export default {
    nav: {
        home: 'maison',
        lines: 'lignes',
        contactUs: 'Contactez nous'
    },
    message: {
        title: 'c\'est un titre',
        paragraph: 'Ceci est un paragraphe',
        email: 'Écrivez-nous à:',
        call: 'ou appelez-nous au:'
    },
    lines: {
        pando: {
            name: 'P&O Cruises',
            desc: 'P & O Cruises associe les dernières innovations à des siècles d\'expérience dans le domaine de la croisière et propose plus de croisières au départ de Southampton que n\'importe quelle compagnie de croisières britannique.'
        },
        royal: {
            name: 'Royal Caribbean',
            desc: 'RCI: la flotte de croisière la plus jeune et la plus innovante au monde. Avec un choix de plus de 20 navires, fournit des croisières vers plus de 130 destinations.'
        },
        princess: {
            name: 'Princess Cruises',
            desc: 'Grâce à leur service amical à la mode américaine, ils prendront soin de tous vos caprices et les pensées de la vie quotidienne chez vous vont bientôt disparaître.'
        },
        cunard: {
            name: 'Cunard Line',
            desc: 'Offrant des voyages hors du temps depuis 1840, Cunard est devenu synonyme de confort et de style. Le légendaire Cunard dépassera vos attentes.'
        }

    }
}