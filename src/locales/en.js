export default {
    nav: {
        home: 'Home',
        lines: 'Lines',
        contactUs: 'Contact us'
    },
    message: {
        title: 'This is a title',
        paragraph: 'This is a paragraph',
        email: 'Email us at:',
        call: 'or call us at:'
    },
    lines: {
        pando: {
            name: 'P&O Cruises',
            desc: 'P&O Cruises combine latest innovations with centuries of cruising expertise and offer more cruises from Southampton than any British cruise line.'
        },
        royal: {
            name: 'Royal Caribbean',
            desc: 'RCI: the world\'s youngest and most innovative cruise fleet. With a choice of more than 20 ships, provides cruises to more than 130 destinations.'
        },
        princess: {
            name: 'Princess Cruises',
            desc: 'With their friendly American-style \'can do\' service, will take care of your every whim and thoughts of everyday life back home will soon melt.'
        },
        cunard: {
            name: 'Cunard Line',
            desc: 'Delivering timeless voyages since 1840, Cunard has become a byword for comfort and style. The legendary Cunard will exceed your expectations.'
        }

    }
}